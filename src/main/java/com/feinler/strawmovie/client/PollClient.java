package com.feinler.strawmovie.client;


import com.feinler.strawmovie.utils.token.AuthenticationFailure;
import com.feinler.strawmovie.utils.token.TokenRequestConnector;


public class PollClient {
    public static void main( String[] args ) {
        PollClient client = new PollClient();
        client.doJob();

    }


    private TokenRequestConnector connector = new TokenRequestConnector( buildURI( "/login" ) );


    public void doJob() {
        try {
            connector.connect( "gael", "gael" );
        } catch ( AuthenticationFailure authenticationFailure ) {
            authenticationFailure.printStackTrace();
        }
    }


    private static String buildURI( String uriSuffix ) {
        return "http://localhost:8080/api" + uriSuffix;
    }


}
